package main

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"os"
	"os/signal"
	"strings"
	"time"
	"unsafe"

	bpf "github.com/iovisor/gobpf/bcc"
	"github.com/iovisor/gobpf/pkg/tracepipe"
	logrus "github.com/sirupsen/logrus"
	logkit "gitlab.com/gitlab-org/labkit/log"
	"gitlab.com/gitlab-org/labkit/monitoring"
	kingpin "gopkg.in/alecthomas/kingpin.v2"
)

import "C"

//go:generate esc -o static.go -pkg main gitalymon.c.bpf
const argSize = 128

type processStartDataEvent struct {
	Pid  uint32
	Argp int32
	Argv [argSize]byte
}

type processExitDataEvent struct {
	Pid         uint32
	BlockReads  uint32
	BlockWrites uint32
}

// TODO: consider loading this from the gitaly config
const repoRoot = "/var/opt/gitlab/git-data/repositories/"

// The maximum BPF kernel map used for communicating between the kernel and the userspace
// parts of this program
const maximumHashSize = 10240

type processInfo struct {
	args []string
}

var childProcesses map[uint32]*processInfo = make(map[uint32]*processInfo, 100)
var exitedProcessesAwaitArgs []processExitDataEvent

var (
	debug      = kingpin.Flag("debug", "Verbose mode.").Short('d').Bool()
	listen     = kingpin.Flag("listen", "Address to listen on, for Prometheus ").OverrideDefaultFromEnvar("LISTEN_ADDR").Short('l').String()
	log        = kingpin.Flag("log", "Emit structured logging events").Short('e').Default("true").Bool()
	repoPrefix = kingpin.Flag("repo-prefix", "Prefix to strip from repo paths").Default("/var/opt/gitlab/git-data/repositories/").String()
	interval   = kingpin.Flag("interval", "Interval at which to poll for exit events").Short('i').Default("1s").Duration()
)

func handleProcessStartEvent(d processStartDataEvent) {
	argv := (*C.char)(unsafe.Pointer(&d.Argv))
	arg := C.GoString(argv)

	if val, ok := childProcesses[d.Pid]; ok {
		val.args = append(val.args, arg)
		return
	}

	if d.Argp > 0 {
		return
	}

	val := &processInfo{args: []string{arg}}
	childProcesses[d.Pid] = val
}

func handleProcessExitEvent(d processExitDataEvent, dequeuing bool) {
	val, ok := childProcesses[d.Pid]
	if !ok {
		if dequeuing {
			logrus.WithFields(logrus.Fields{"pid": d.Pid}).Info("unknown process")
		} else {
			// We have recieved notification of the process exiting before
			// recieving the arguments. Wait until the next cycle before
			// analysis
			exitedProcessesAwaitArgs = append(exitedProcessesAwaitArgs, d)
		}
		return
	}
	delete(childProcesses, d.Pid)

	cmdCategory, gitDir := analyseArguments(val.args)

	commandsCompleted.WithLabelValues(cmdCategory).Inc()
	blockReads.WithLabelValues(cmdCategory).Observe(float64(d.BlockReads))
	blockWrites.WithLabelValues(cmdCategory).Observe(float64(d.BlockWrites))

	if *log {
		fields := logrus.Fields{
			"cmd":          strings.Join(val.args, " "),
			"cmd_category": cmdCategory,
			"block_reads":  d.BlockReads,
			"block_writes": d.BlockWrites,
			"total_bio":    d.BlockReads + d.BlockWrites,
		}

		if gitDir != "" {
			fields["repo_path"] = strings.TrimPrefix(gitDir, *repoPrefix)
		}

		logrus.WithFields(fields).Info("cmd exit")
	}
}

func handleProcessExits(table *bpf.Table) {
	// First process race-condition exited processes from last cycle...
	if len(exitedProcessesAwaitArgs) > 0 {
		for _, event := range exitedProcessesAwaitArgs {
			handleProcessExitEvent(event, true)
		}
		exitedProcessesAwaitArgs = nil
	}

	count := 0
	for it := table.Iter(); it.Next(); {
		count++
		event := processExitDataEvent{}
		key := it.Key()

		err := binary.Read(bytes.NewBuffer(it.Leaf()), bpf.GetHostByteOrder(), &event)
		if err != nil {
			logrus.WithError(err).Warn("Unable to read process exit table")
		}
		handleProcessExitEvent(event, false)
		table.Delete(key)
	}

	if count >= int(maximumHashSize*0.95) {
		logrus.Warn("Warning: eBPF event table near maximum size. Consider reducing the interval parameter")
	}
}

func attachKprobe(m *bpf.Module, fnName string, kprobeName string) error {
	fd, err := m.LoadKprobe(fnName)
	if err != nil {
		return err
	}

	// passing -1 for maxActive signifies to use the default
	// according to the kernel kprobes documentation
	// NOTE: when using -1, the proble is not being removed:
	// https://github.com/iovisor/gobpf/issues/174
	err = m.AttachKprobe(kprobeName, fd, 32)
	return err
}

func attachTracepoint(m *bpf.Module, fnName string, tracepointName string) error {
	fd, err := m.LoadTracepoint(fnName)
	if err != nil {
		return err
	}

	err = m.AttachTracepoint(tracepointName, fd)
	return err
}

func main() {
	kingpin.Parse()

	// Initialize the global logger
	closer, err := logkit.Initialize(
		logkit.WithFormatter("json"),
		logkit.WithLogLevel("info"),
	)
	defer closer.Close()

	if listen != nil {
		registerMetrics()

		// Start listener
		go func() {
			logrus.WithError(monitoring.Serve(
				monitoring.WithListenerAddress(*listen),
			)).Fatal("Unable to start monitor")
		}()
	}

	debugInt := 0
	if *debug {
		debugInt = 1
	}

	m := bpf.NewModule(FSMustString(false, "/gitalymon.c.bpf"), []string{
		fmt.Sprintf("-DARGSIZE=%d", argSize),
		fmt.Sprintf("-DMAXIMUM_HASH_SIZE=%d", maximumHashSize),
		fmt.Sprintf("-DPERF_TOOLS_DEBUG=%", debugInt),
	})
	if m == nil {
		logrus.Fatal("Failed to load eBPF module")
	}
	defer m.Close()

	err = attachKprobe(m, "syscall__execve", bpf.GetSyscallFnName("execve"))
	if err != nil {
		logrus.WithError(err).Fatal("Failed to attach execve kprobe")
	}

	err = attachKprobe(m, "trace_blk_account_io_start", "blk_account_io_start")
	if err != nil {
		logrus.WithError(err).Fatal("Failed to attach blk_account_io_start kprobe")
	}

	err = attachTracepoint(m, "tracepoint_sched_process_exit_handler", "sched:sched_process_exit")
	if err != nil {
		logrus.WithError(err).Fatal("Failed to attach tracepoint sched:sched_process_exit")
	}

	processStartEventsTable := bpf.NewTable(m.TableId("process_start_events"), m)
	processStartChannel := make(chan []byte)
	processStartEventsMap, err := bpf.InitPerfMap(processStartEventsTable, processStartChannel)
	if err != nil {
		logrus.WithError(err).Fatal("Failed to init perf map")
	}

	exitedProcessesTable := bpf.NewTable(m.TableId("exited_processes"), m)

	if *debug {
		tp, err := tracepipe.New()
		if err != nil {
			logrus.WithError(err).Fatal("Failed to open trace pipe")
		}
		defer tp.Close()
		tpChannel, tpErrorChannel := tp.Channel()

		go func() {
			for {
				select {
				case event := <-tpChannel:
					logrus.WithField("event", event).Info("tracepipe")

				case err := <-tpErrorChannel:
					logrus.WithError(err).Info("tracepipe")
				}
			}
		}()
	}

	go func() {
		ticker := time.NewTicker(*interval)

		for {
			// give the ticket priority, with a non-blocking select first
			select {
			case <-ticker.C:
				handleProcessExits(exitedProcessesTable)
			default:
			}

			select {
			case data := <-processStartChannel:
				var event processStartDataEvent
				err := binary.Read(bytes.NewBuffer(data), bpf.GetHostByteOrder(), &event)
				if err != nil {
					logrus.WithError(err).Fatal("failed to decode received data")
					continue
				}
				handleProcessStartEvent(event)

			case <-ticker.C:
				handleProcessExits(exitedProcessesTable)
			}
		}
	}()

	sig := make(chan os.Signal, 1)
	signal.Notify(sig, os.Interrupt, os.Kill)

	processStartEventsMap.Start()
	defer processStartEventsMap.Stop()

	<-sig
	logrus.Info("Terminating")
}
