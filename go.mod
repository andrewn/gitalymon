module gitlab.com/gitlab-org/gitalymon

go 1.13

require (
	github.com/iovisor/gobpf v0.0.0-20191017091429-c3024dcc6881
	github.com/mjibson/esc v0.2.0 // indirect
	github.com/prometheus/client_golang v1.0.0
	github.com/sirupsen/logrus v1.3.0
	gitlab.com/gitlab-org/labkit v0.0.0-20190902063225-3253d7975ca7
	golang.org/x/crypto v0.0.0-20191011191535-87dc89f01550 // indirect
	gopkg.in/alecthomas/kingpin.v2 v2.2.6
)
