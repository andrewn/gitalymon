package main

import "strings"

const maxAnalysedArguments = 10

func replaceArgWithPlaceholder(arg string) string {
	if strings.HasPrefix(arg, "refs/") {
		return "[refs/...]"
	}

	if strings.HasPrefix(arg, "/") {
		return "[/path]"
	}

	isSha := true
	isNumber := true

	for _, v := range arg {
		if !(v >= '0' && v <= '9') {
			isNumber = false
		}

		if !(v >= '0' && v <= '9' || v >= 'a' && v <= 'f' || v >= 'A' && v <= 'F') {
			isSha = false
		}

		if !isNumber && !isSha {
			return "[arg]"
		}
	}

	if isNumber {
		return "[num]"
	}

	if isSha {
		return "[sha]"
	}

	return "[arg]"
}

func analyseArguments(args []string) (cmdCategory string, gitDir string) {
	maxLen := len(args)
	if maxLen > maxAnalysedArguments {
		maxLen = maxAnalysedArguments
	}

	categorisedCmd := make([]string, maxLen)
	subcommandFound := false
	nextIsParam := false

	for k, v := range args {
		// Always add the first argument
		if k == 0 {
			categorisedCmd[0] = v
			continue
		}

		// Try find the git dir, if we haven't already
		if gitDir == "" && strings.HasPrefix(v, repoRoot) {
			gitDir = v
		}

		// If we're passed the max params, only continue if
		// we're still searching for the gitDir
		if k >= maxLen {
			if gitDir == "" {
				continue
			} else {
				break
			}
		}

		// Some parameters will be followed by an argument
		if v == "-c" {
			categorisedCmd[k] = v
			nextIsParam = true
			continue
		}

		if nextIsParam {
			nextIsParam = false
			components := strings.SplitAfterN(v, "=", 2)
			if len(components) == 1 {
				categorisedCmd[k] = replaceArgWithPlaceholder(v)
			} else {
				categorisedCmd[k] = components[0] + replaceArgWithPlaceholder(components[1])
			}
			continue
		}

		if v == "--" {
			categorisedCmd[k] = "--"
			break // Skip remaining values
		}

		if v == "" || v == " " {
			categorisedCmd[k] = v
			continue
		}

		if strings.HasPrefix(v, "-") {
			components := strings.SplitAfterN(v, "=", 2)
			if len(components) == 1 {
				categorisedCmd[k] = v
			} else {
				categorisedCmd[k] = components[0] + replaceArgWithPlaceholder(components[1])
			}
			continue
		}

		// Some commands are "categorizable", but not the subcommand
		if v == "exec" || v == "bin/ruby-cd" {
			categorisedCmd[k] = v
			continue
		}

		replacement := replaceArgWithPlaceholder(v)
		if replacement == "[arg]" && !subcommandFound {
			categorisedCmd[k] = v
			subcommandFound = true
			continue
		}

		categorisedCmd[k] = replacement
	}

	cmdCategory = strings.Join(categorisedCmd, " ")
	return cmdCategory, gitDir
}
