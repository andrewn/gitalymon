package main

import "github.com/prometheus/client_golang/prometheus"

var commandsCompleted = prometheus.NewCounterVec(
	prometheus.CounterOpts{
		Subsystem: "gitalymon",
		Name:      "exec_finished_total",
		Help:      "Total number of completed commands",
	},
	[]string{"cmd_category"},
)

var blockReads = prometheus.NewHistogramVec(
	prometheus.HistogramOpts{
		Subsystem: "gitalymon",
		Name:      "block_read_ops",
		Help:      "Block read operations",
		Buckets:   []float64{0, 1, 10, 100, 1000, 10000},
	},
	[]string{"cmd_category"},
)

var blockWrites = prometheus.NewHistogramVec(
	prometheus.HistogramOpts{
		Subsystem: "gitalymon",
		Name:      "block_write_ops",
		Help:      "Block write operations",
		Buckets:   []float64{0, 1, 10, 100, 1000, 10000},
	},
	[]string{"cmd_category"},
)

func registerMetrics() {
	prometheus.MustRegister(commandsCompleted)
	prometheus.MustRegister(blockReads)
	prometheus.MustRegister(blockWrites)
}
