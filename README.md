# Gitalymon

Gitalymon is a eBPF monitoring tool for GitLab's Gitaly component.

Gitaly spawns git processes to carry most of its work. In many cases, these git processes will
spawn child, and grandchild, processes.

This makes accounting tricky, but eBPF can help by tracking the heirachy of child processes and monitoring them
as a single request.

## Observability

The tool then attempts to categorise each git command, so that analsys and aggregation can take place.

It exposes these metrics as Prometheus metrics and structured logs, which can be ingested by something like ELK.

## Running

```console
./gitalymon --help
usage: gitalymon [<flags>]

Flags:
      --help           Show context-sensitive help (also try --help-long and --help-man).
  -d, --debug          Verbose mode.
  -l, --listen=LISTEN  Address to listen on, for Prometheus
  -e, --log            Emit structured logging events
      --repo-prefix="/var/opt/gitlab/git-data/repositories/"
                       Prefix to strip from repo paths
  -i, --interval=1s    Interval at which to poll for exit events
```

## Example Output

### Logs

By default `gitalymon` will emit structured logs:

```
./gitalymon
{"block_reads":3,"block_writes":0,"cmd":"/opt/gitlab/embedded/bin/git --git-dir /var/opt/gitlab/git-data/repositories/@hashed/9a/b4/9ab4e3f3e73c750e7a4972dea50a2b56235769d7d7f8aec8696ec416b3e40304.git for-each-ref --format=%(refname) refs/heads ","cmd_category":"/opt/gitlab/embedded/bin/git --git-dir [/path] for-each-ref --format=[arg] [refs/...] ","level":"info","msg":"cmd exit","repo_path":"@hashed/9a/b4/9ab4e3f3e73c750e7a4972dea50a2b56235769d7d7f8aec8696ec416b3e40304.git","time":"2019-12-20T09:24:24Z","total_bio":3}
{"block_reads":2,"block_writes":0,"cmd":"/opt/gitlab/embedded/bin/git --git-dir /var/opt/gitlab/git-data/repositories/@hashed/58/cc/58cc47719cc0c02aa45e8642267bd26dbfa729120e1c02ef3a1ee2ce764894d8.git for-each-ref --format=%(refname) refs/tags ","cmd_category":"/opt/gitlab/embedded/bin/git --git-dir [/path] for-each-ref --format=[arg] [refs/...] ","level":"info","msg":"cmd exit","repo_path":"@hashed/58/cc/58cc47719cc0c02aa45e8642267bd26dbfa729120e1c02ef3a1ee2ce764894d8.git","time":"2019-12-20T09:24:24Z","total_bio":2}
{"block_reads":3,"block_writes":0,"cmd":"/opt/gitlab/embedded/bin/git --git-dir /var/opt/gitlab/git-data/repositories/@hashed/07/7f/077f18a338d8e87c40ccf17fb0623ae1e744417595e744e8eba6c82283ba8365.git for-each-ref --format=%(refname) refs/heads ","cmd_category":"/opt/gitlab/embedded/bin/git --git-dir [/path] for-each-ref --format=[arg] [refs/...] ","level":"info","msg":"cmd exit","repo_path":"@hashed/07/7f/077f18a338d8e87c40ccf17fb0623ae1e744417595e744e8eba6c82283ba8365.git","time":"2019-12-20T09:24:24Z","total_bio":3}
{"block_reads":2,"block_writes":0,"cmd":"/opt/gitlab/embedded/bin/git --git-dir /var/opt/gitlab/git-data/repositories/@hashed/8d/02/8d02b067896fe34248694614c6808eee4586b041cf09416b727b97c22a4d7bdd.git for-each-ref --format=%(refname) refs/tags ","cmd_category":"/opt/gitlab/embedded/bin/git --git-dir [/path] for-each-ref --format=[arg] [refs/...] ","level":"info","msg":"cmd exit","repo_path":"@hashed/8d/02/8d02b067896fe34248694614c6808eee4586b041cf09416b727b97c22a4d7bdd.git","time":"2019-12-20T09:24:24Z","total_bio":2}
{"block_reads":3,"block_writes":0,"cmd":"/opt/gitlab/embedded/bin/git --git-dir /var/opt/gitlab/git-data/repositories/@hashed/5d/07/5d076a31eb737f92e5eb056146996898d1dce45bf116c0d65fdfee0feea8b311.git for-each-ref --format=%(refname) refs/heads ","cmd_category":"/opt/gitlab/embedded/bin/git --git-dir [/path] for-each-ref --format=[arg] [refs/...] ","level":"info","msg":"cmd exit","repo_path":"@hashed/5d/07/5d076a31eb737f92e5eb056146996898d1dce45bf116c0d65fdfee0feea8b311.git","time":"2019-12-20T09:24:24Z","total_bio":3}
{"block_reads":26,"block_writes":7,"cmd":"/opt/gitlab/embedded/bin/git -c core.hooksPath=/opt/gitlab/embedded/service/gitaly-ruby/git-hooks -c core.alternateRefsCommand=exit 0 # -c receive.maxInputSize=10485760000 receive-pack --stateless-rpc /var/opt/gitlab/git-data/repositories/@hashed/bc/0d/bc0d224f20fdf24752c10be309cb701ed93bbaee99b3b63a519b30bca96a48b1.git ","cmd_category":"/opt/gitlab/embedded/bin/git -c core.hooksPath=[/path] -c core.alternateRefsCommand=[arg] -c receive.maxInputSize=[num] receive-pack --stateless-rpc [/path]","level":"info","msg":"cmd exit","repo_path":"@hashed/bc/0d/bc0d224f20fdf24752c10be309cb701ed93bbaee99b3b63a519b30bca96a48b1.git","time":"2019-12-20T09:24:24Z","total_bio":33}
```

### Metrics

`gitalymon` can be configured to serve Prometheus metrics on a given port:

```
sudo ./gitalymon -l :9299 --no-log &
```

`gitalymon` will produce metrics with these names:

```
gitalymon_block_read_ops_bucket
gitalymon_block_read_ops_count
gitalymon_block_read_ops_sum

gitalymon_block_write_ops_bucket
gitalymon_block_write_ops_count
gitalymon_block_write_ops_sum

gitalymon_exec_finished_total
```

All metrics have a `cmd_category` dimension, for example:

```
gitalymon_block_read_ops_sum{cmd_category="/opt/gitlab/embedded/bin/git --git-dir [/path] for-each-ref --format [arg] [refs/...] "} 10
```
